﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CriticalRoleAnalyzer
{
    public class Player
    {
        public string PlayerName { get; }
        public IReadOnlyList<string> AllNames { get; }
        public IReadOnlyList<string> AllNamesLower { get; }
        public int Swears { get; private set; }
        public List<int> SwearLines { get; private set; }
        public int Intensifiers { get; private set; }
        public int Apologies { get; private set; }

        public Player(string playerName, params string[] characterNames)
        {
            SwearLines = new List<int>();
            PlayerName = playerName;
            AllNames = characterNames.ToList().Concat(new List<string>() { PlayerName }).ToList();
            AllNamesLower = AllNames.Select(x => x.ToLower() + ":").ToList();
        }

        public void Swear(int times, int lineNumber)
        {
            Swears += times;
            for (int i = 0; i < times; i++) SwearLines.Add(lineNumber);
        }

        public void Intensify(int times = 1)
        {
            Intensifiers += times;
        }

        public void Apologize(int times = 1)
        {
            Apologies += times;
        }

        public bool HasName(string name)
        {
            return AllNames.Select(x => x.ToLower()).Contains(name.ToLower());
        }
    }
}
