﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CriticalRoleAnalyzer
{
    public class Tracker
    {
        public IReadOnlyList<Player> Players { get; }
        private Player _currentPlayer;
        private readonly List<string> _swears;
        private readonly List<string> _apologies;
        private readonly List<string> _intensifiers;
        private readonly List<string> _characterNames;
        private int lineNumber = 0;

        public void TrackLine(string line)
        {
            lineNumber++;
            var lowerLine = line.ToLower();
            var matchingNames = _characterNames.Where(x => lowerLine.Contains(x)).ToList();
            if (matchingNames.Count > 0)
            {
                var matchingName = matchingNames.FirstOrDefault(x => lowerLine.Substring(0, x.Length) == x);
                if (matchingName != null)
                {
                    _currentPlayer = Players.First(x => x.AllNamesLower.Contains(matchingName));
                }
            }

            _currentPlayer?.Swear(CountAppearancesOfStrings(lowerLine, _swears), lineNumber);
            _currentPlayer?.Apologize(CountAppearancesOfStrings(lowerLine, _apologies));
            _currentPlayer?.Intensify(CountAppearancesOfStrings(lowerLine, _intensifiers));
        }

        private int CountAppearancesOfStrings(string line, List<string> checkForThese) =>
            checkForThese.Select(x => Regex.Matches(Regex.Escape(line), x).Count).Sum();

        public Tracker()
        {
            Players = new List<Player>()
            {
                new Player("Sam Riegel", "Sam", "Scanlan", "Scanlan Shorthalt", "Taryon", "Taryon Darrington"),
                new Player("Matthew Mercer", "Matt", "Matthew", "Matthew Mercer", "DM", "GM"),
                new Player("Marisha Ray", "Marisha", "Keyleth"),
                new Player("Taliesin Jaffe", "Taliesin", "Percy", "Percy de Rolo", "Percy de Rolo III"),
                new Player("Liam O'Brien", "Liam", "Vax", "Vax'ildan"),
                new Player("Laura Bailey", "Laura", "Vex", "Vex'ahlia"),
                new Player("Travis Willingham", "Travis", "Grog", "Grog Strongjaw"),
                new Player("Ashley Johnson", "Ashley", "Pike", "Pike Trickfoot"),
                new Player("Orion Acaba", "Orion", "Tiberius"),
                new Player("Mary Elizabeth McGlynn", "Mary", "Mary Elizabeth", "Zahra"),
                new Player("Will Friedle", "Will", "Kashaw"),
                new Player("Patrick Rothfuss", "Patrick", "Pat", "Kerrek"),
                new Player("Wil Wheaton", "Wil", "Thorbir"),
                new Player("Felicia Day", "Felicia", "Lyra"),
                new Player("Ashly Burch", "Ashly", "Ashley", "Ashley Birch", "Dren"),
                new Player("Noelle Stevenson", "Noelle", "Noel", "Tova"),
                new Player("Kit Buss", "Kit", "Kitt", "Lillith"),
                new Player("Dan Casey", "Dan", "Salty Pete"),
                new Player("Zachary K. Eubank", "Zachary", "Zach", "Snugglelord"),
                new Player("Ify Nwadiwe", "Ify", "Ulfgar"),
                new Player("Chris Hardwick", "Chris", "Gern Blanston", "Gern"),
                new Player("Phil Lamarr", "Phil", "Browntooth"),
                new Player("Jason Miller", "Jason", "Garthok"),
                new Player("Ivan Van Norman", "Ivan", "Grizznak"),
                new Player("Jon Heder", "Jon", "Lionel Gayheart", "Chod")
            };

            _swears = new List<string>()
            {
                "fuck",
                "shit",
                "damn",
                "bitch",
                "cunt",
                "ass",
                "dammit",
                "bastard"
            };

            _intensifiers = new List<string>()
            {
                " so ",
                " very ",
                " really ",
                " extremely ",
                " incredibly "
            };

            //_intensifiers = _intensifiers.SelectMany(x => new List<string> { x + " ", x + ".", x + ",", x + "!", x + "?" }).ToList();

            _apologies = new List<string>()
            {
                " sorry ",
                " excuse me ",
                " my bad "
            };

            //_apologies = _intensifiers.SelectMany(x => new List<string> { x + " ", x + ".", x + ",", x + "!", x + "?" }).ToList();

            _characterNames = Players.SelectMany(x => x.AllNames).Select(x => x.ToLower() + ":").ToList();
        }
    }
}
