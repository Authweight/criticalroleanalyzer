﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CriticalRoleAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Too many or not enough arguments. Specify an input file and an output file");
                return;
            }

            Tracker tracker = new Tracker();

            string[] lines = File.ReadAllLines(args[0]);
            lines.ToList().ForEach(x => tracker.TrackLine(x));
            string[] outLines = tracker.Players
                .Select(x => x.PlayerName + " - " + $"Swears: {x.Swears}, Intensifiers: {x.Intensifiers}, Apologies: {x.Apologies}. Swear lines: {x.SwearLines.Select(y => y.ToString()).Concat(new List<string>() { "" }).Aggregate((z, w) => z + ", " + w) }")
                .ToArray();

            File.WriteAllLines(args[1], outLines);
        }
    }
}
