﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CriticalRoleAnalyzer;
using FluentAssertions;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace TrackerTests
{
    [TestFixture]
    class TrackerTests
    {
        [Test]
        public void TracksSingleSwearInLine()
        {
            //ARRANGE
            var sut = new Tracker();

            //ACT
            sut.TrackLine("Matt: fuck you");

            //ASSERT
            sut.Players.First(x => x.PlayerName == "Matthew Mercer").Swears.Should().Be(1);
        }

        [Test]
        public void TracksMultipleLinesWithSwears()
        {
            //ARRANGE
            var sut = new Tracker();

            //ACT
            sut.TrackLine("Matt: fuck you");
            sut.TrackLine("i fucking hate u");

            //ASSERT
            sut.Players.First(x => x.PlayerName == "Matthew Mercer").Swears.Should().Be(2);
        }

        [Test]
        public void TracksMultipleSwearsInSingleLine()
        {
            //ARRANGE
            var sut = new Tracker();

            //ACT
            sut.TrackLine("Matt: fuck you. i hope u die in a fire, you huge fucking jackass");

            //ASSERT
            sut.Players.First(x => x.PlayerName == "Matthew Mercer").Swears.Should().Be(3);
        }

        [Test]
        public void TracksSwearsForMultiplePlayers()
        {
            //ARRANGE
            var sut = new Tracker();

            //ACT
            sut.TrackLine("Matt: fuck you");
            sut.TrackLine("i fucking hate u");
            sut.TrackLine("Vex: i fucking hate u too");

            //ASSERT
            sut.Players.First(x => x.PlayerName == "Matthew Mercer").Swears.Should().Be(2);
            sut.Players.First(x => x.PlayerName == "Laura Bailey").Swears.Should().Be(1);
        }

        [Test]
        public void DoesNotSwitchPlayerForNameAppearingInMiddleOfLine()
        {
            //ARRANGE
            var sut = new Tracker();

            //ACT
            sut.TrackLine("Matt: fuck you. Grog: go fuck yourself");
            sut.TrackLine("Grog: okay fuck you too");

            //ASSERT
            sut.Players.First(x => x.PlayerName == "Matthew Mercer").Swears.Should().Be(2);
            sut.Players.First(x => x.PlayerName == "Travis Willingham").Swears.Should().Be(1);
        }

        [Test]
        public void MarishaApologizesAndSwears()
        {
            //ARRANGE
            var sut = new Tracker();
            string line = "Marisha: When nothing happens, he asks Scanlan if the others are fucking with him. They admit that they are, but assure him it means they like him. Sorry.";

            //ACT
            sut.TrackLine("Marisha: When nothing happens, he asks Scanlan if the others are fucking with him. They admit that they are, but assure him it means they like him. Sorry.");

            //ASSERT
            var marisha = sut.Players.First(x => x.PlayerName == "Marisha Ray");
            marisha.Swears.Should().Be(1);
            marisha.Apologies.Should().Be(1);
        }
    }
}
